import '../../index.css'
import ChangeCounter from '../changeCounter/changeCounter';

const RndCounter = () => {
    const { count, RND, RESET } = ChangeCounter();
    
    return (
        <div className="component">
            <div className="counter">{count}</div>
            <div className="controls">
                <button onClick={RND} >RND</button>
                <button onClick={RESET} >RESET</button>    
            </div>
        </div>)}

export default RndCounter;