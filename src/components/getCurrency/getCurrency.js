import {  useState, useEffect }  from "react";


const GetCurrency = () => {

  const [ valutes, setValutes ] = useState('');
  
    useEffect(() => {
    //делаем запрос на получение всех валют
    fetch('https://www.cbr-xml-daily.ru/daily_json.js')
      .then(res => res.json())
      .then(res => setValutes(res.Valute))
      .catch(errorMessage => console.log(errorMessage))
    }, []);

  return valutes 
}
  
export default GetCurrency;
