import '../../index.css'
import ChangeCounter from '../changeCounter/changeCounter';
import React from 'react';

const Counter = () => {  
    const { count, INC, DEC, RESET, RND } = ChangeCounter();
    

    return (
      <div className="component">
        <div className="counter">{count}</div>
      <div className="controls">
        <button onClick={INC} >INC</button>
        <button onClick={DEC} >DEC</button>
        <button onClick={RND} >RND</button>
        <button onClick={RESET} >RESET</button>  
          
        </div>
      </div>
      
    )}

    export default Counter;