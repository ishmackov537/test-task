import { useState, useEffect } from 'react'; 
import '../../index.css'
//import CurrencyConverter from "../currencyConverter/currencyConverter";
import GetCurrency from '../getCurrency/getCurrency';

const Converter = () => {
    //const { result, invalue, setInvalue, toConvert, setSelectResult, setSelectInvalue, usd, eur, gbp } = CurrencyConverter();
    const valutes = GetCurrency();
    const { USD, EUR, GBP } = valutes;

    const [valuteFirst, setValuteFirst] = useState(1)
    const [valuteSecond, setValuteSecond] = useState(1)
    const [value, setValue] = useState(1)
    const [result, setResult] = useState(1)
  
    useEffect(() => {
      setResult((value*valuteFirst/valuteSecond).toFixed(2))
    },[valuteFirst, valuteSecond, value])
    
    return (
      <div className="component-select">
        <form>
          <div>
            <div className="courses">
              <div className="course-item-body">
              <div className="course-item-title">{USD?.CharCode}</div>
                  <div className="course-item-value" data-value="USD">{USD?.Value.toFixed(2)}</div>
              </div>
              <div className="course-item-body">
                  <div className="course-item-title">{EUR?.CharCode}</div>
                  <div className="course-item-value" data-value="EUR">{EUR?.Value.toFixed(2)}</div>
              </div >
              <div className="course-item-body">
                  <div className="course-item-title">{GBP?.CharCode}</div>
                  <div className="course-item-value" data-value="GBP">{GBP?.Value.toFixed(2)}</div>
              </div>
          </div>
            <select
              className="form-control"
              onChange={(event)=>setValuteFirst(event.target.value)}
              >
              <option value="1" > RUB </option>
              {Object.values(valutes).map(valute => (
                <option value={valute.Value} >{valute.CharCode}</option>
              ))}
            </select>
            <select
              id="select"
              className="form-control"
              onChange={(event)=>setValuteSecond(event.target.value)}
              >
               {Object.values(valutes).map(valute => (
                <option value={valute.Value} >{valute.CharCode}</option>
              ))}
            </select>
          </div>
          <div>
            <input
              id="input"
              type="number"
              className="form-control"
              value={value}
              onChange={(event)=>setValue(event.target.value)}
              />
            <input
              id="result"
              type="number"
              className="form-control"
              value={result}
              />
          </div>
        </form>
      </div>)}

export default Converter;