import React from 'react';

const ChangeCounter = () => {
    const [count, setCount] = React.useState(0);
    
    console.log("start_func")

    React.useEffect(() => {
      RND();
      //console.log("start")
      },[]);
    
    const INC = () => {
      setCount((prevState) => prevState + 1);
    }
    
    const DEC = () => {
      setCount((prevState) => prevState - 1);
    }
    
    const RND = () => {
      fetch('https://www.random.org/integers/?num=1&min=-50&max=50&col=1&base=10&format=plain&rnd=new')
        .then(res => res.text())
        .then(res => setCount(Number(res)))
    }
    
    const RESET = () => {
      setCount(0);
    }
    
    return {count, INC, DEC, RND, RESET}
  }

  export default ChangeCounter; 