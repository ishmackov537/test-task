import { useState } from "react";
//import GetCurrency from "../getCurrency";


const CurrencyConverter = () => {
    const [ result, setResult ] = useState('');
    const [ invalue, setInvalue ] = useState('');
    const [ selectInvalue, setSelectInvalue ] = useState("RUB");
    const [ selectResult, setSelectResult ] = useState("USD");
    const [ usd, setUsd ] = useState(0);
    const [ eur, setEur ] = useState(0);
    const [ gbp, setGbp ] = useState(0);
    const [ getData, setGetData ] = useState(false);
    
    
    if (getData === false) {
        fetch('https://www.cbr-xml-daily.ru/daily_json.js')
        .then(res => res.json())
        .then(res => setUsd(res.Valute.USD.Value))
        .catch(errorMessage => console.log(errorMessage))
    
        fetch('https://www.cbr-xml-daily.ru/daily_json.js')
        .then(res => res.json())
        .then(res => setEur(res.Valute.EUR.Value))
        .catch(errorMessage => console.log(errorMessage))
    
        fetch('https://www.cbr-xml-daily.ru/daily_json.js')
        .then(res => res.json())
        .then(res => setGbp(res.Valute.GBP.Value))
        .catch(errorMessage => console.log(errorMessage))
        setGetData(current => !current);

    }

    const toConvert = () => {
      switch(selectInvalue){
       
        case "RUB":{
          switch(selectResult) {
            case "RUB":{
              setResult((invalue/1).toFixed(2));
              break;
            }
            case "USD":{
              setResult((invalue/usd).toFixed(2));
              break;
            }
            case "EUR": { 
              setResult((invalue/eur).toFixed(2));
              break;
            }
            case "GPB": {
              setResult((invalue/gbp).toFixed(2));
              break;
              }
            default:
            // do nothing
          }
          break;
        }
        case "USD" : {
          switch(selectResult) {
            case "RUB":{
              setResult((invalue*usd).toFixed(2));
              break;
            }
            case "USD": {
              setResult((invalue*1).toFixed(2));
              break;
            }
            case "EUR": {
              setResult((invalue*usd/eur).toFixed(2));
              break;
            }
            case "GPB": {
              setResult((invalue*usd/gbp).toFixed(2));
              break; 
              }
            default:
            // do nothing
          }
          break;
        }
          case "EUR" : {
          switch(selectResult) {
            case "RUB":{
              setResult((invalue*eur).toFixed(2));
              break;
            }
            case "USD": {
              setResult((invalue*eur/usd).toFixed(2));
              break;
            }
            case "EUR": {
              setResult((invalue*1).toFixed(2));
              break;
            }
            case "GPB": {
              setResult((invalue*eur/gbp).toFixed(2));
              break; 
              }
            default:
            // do nothing
          }
          break;
        }
        case "GPB" : {
          switch(selectResult) {
            case "RUB":{
              setResult((invalue*gbp).toFixed(2));
              break;
            }
            case "USD": {
              setResult((invalue*gbp/usd).toFixed(2));
              break;
            }
            case "EUR": {
              setResult((invalue*gbp/eur).toFixed(2));
              break;
            }
            case "GPB": {
              setResult((invalue*1).toFixed(2));
              break; 
              }
            default:
            // do nothing
          }
          break;
        }        
        default:
        // do nothing  
      }
    }
  
    
    return { result, invalue, setInvalue, toConvert, setSelectResult, setSelectInvalue, usd, eur, gbp }
    }

export default CurrencyConverter;