import React from 'react';
import { Routes, Route, Link } from 'react-router-dom';
import Counter from '../counter/counter';
import RndCounter from '../rndCounter/rndCounter';
import Converter from '../converter/converter';

const App = () => {
    return (
        <>
            <header className='header'>
                <Link className='link' to="/">Converter </Link>
                <Link className='link' to="/counter">Counter </Link>
                <Link className='link' to="/rnd-counter">RndCounter</Link>
            </header>
            
            <Routes>
                <Route path="/" element={<Converter />}/>
                <Route path="/counter" element={<Counter />}/>
                <Route path="/rnd-counter" element={<RndCounter />}/>
            </Routes>
        
        </>  
       
    )
}

export default App;